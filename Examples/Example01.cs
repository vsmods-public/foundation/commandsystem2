namespace Examples
{
    using System;
    using System.Threading.Tasks;
    using CommandSystem2.Attributes;
    using CommandSystem2.Entities.VSCommand;

    public class Example01
    {
        [VSCommand]
        public async Task HelloWorld(CommandContext context)
        {
            context.ServerPlayer.SendMessage(context.GroupId, "Hello world!", Vintagestory.API.Common.EnumChatType.CommandSuccess);
        }
    }
}
