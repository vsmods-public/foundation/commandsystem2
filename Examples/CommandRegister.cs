namespace Examples
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Text;
    using CommandSystem2;
    using Vintagestory.API.Common;
    using Vintagestory.API.Server;

#if !DEBUG
    public class CommandRegister
#else
    public class CommandRegister : ModSystem
#endif
    {
        CommandSystem serversideCommands = null;
#if !DEBUG
        public void StartServerSide(ICoreServerAPI api)
#else
        public override void StartServerSide(ICoreServerAPI api)
#endif
        {
            var config = new CommandConfig();
            this.serversideCommands = new CommandSystem(config, api);
            this.serversideCommands.RegisterCommands(Assembly.GetAssembly(typeof(CommandRegister)));
        }
    }
}
