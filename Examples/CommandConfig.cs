namespace Examples
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CommandSystem2.Entities.Interfaces;
    using Microsoft.Extensions.DependencyInjection;
    using Newtonsoft.Json;

    public class CommandConfig : ICommandSystemConfig
    {
        [JsonIgnore]
        public IServiceProvider Services { get; set; } = new ServiceCollection().BuildServiceProvider(true);
        [JsonIgnore]
        public bool IgnoreExtraArguments => false;
    }
}
