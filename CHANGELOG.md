## 0.3.10 (2021-08-20)

### Bug fix (3 changes)

- [The exthelp command now handles commands being registered on only one side way better.](vsmods-public/foundation/commandsystem2@d0defbf61abc72b29a0e4b237996959f67999478)
- [HelpCommandSystem now properly includes registered commands](vsmods-public/foundation/commandsystem2@1def3d3db03deff2d0af0d3a6a228b735901688f)
- [Add null check for config in constructor of CommandSystem](vsmods-public/foundation/commandsystem2@f919c1ac1acb77b971a895cafeb18e61ea5298cb)

## 0.3.8 (2021-08-19)

### New feature (1 change)

- [Added examples project for documentation purposes.](vsmods-public/foundation/commandsystem2@b007eea48e4a1de5b2ea6006efca223930df1ea5)

## 0.3.7 (2021-08-19)

### Bug fix (1 change)

- [Fix default arguments for commands](vsmods-public/foundation/commandsystem2@c5cfb7e4aff44415de8bf0a90da508d315330f57)

## 0.3.5 (2021-07-28)

### New feature (1 change)

- [CommandSystem exthelp command now supports never being loaded on the client](vsmods-public/foundation/commandsystem2@0eacd22151f5ca7818575791a33e1f390ef5dc6c)

## 0.3.4 (2021-07-28)

### New feature (1 change)

- [ServerPlayer and ClientPlayer now have side specific types for easy referencing](vsmods-public/foundation/commandsystem2@729e5849cf7d76032196bbf7ef6f7a4d8b0c18e6)

## 0.3.2 (2021-07-28)

### New feature (1 change)

- [Add server API and client API to CommandContext for easy referencing](vsmods-public/foundation/commandsystem2@f0e2228393e9f349011027f704fea55b1c930b7a)
