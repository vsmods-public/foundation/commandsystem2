namespace CommandSystem2.Exceptions
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using CommandSystem2.Attributes.CheckAttributes;
    using CommandSystem2.Entities.VSCommand;

    /// <summary>
    /// Indicates that one or more checks for given command have failed.
    /// </summary>
    public class VSCommandChecksFailException : Exception
    {
        /// <summary>
        /// Gets the command that was executed.
        /// </summary>
        public Command Command { get; }

        /// <summary>
        /// Gets the context in which given command was executed.
        /// </summary>
        public CommandContext Context { get; }

        /// <summary>
        /// Gets the checks that failed.
        /// </summary>
        public IReadOnlyList<VSCommandCheckBaseAttribute> FailedChecks { get; }

        /// <summary>
        /// Creates a new <see cref="VSCommandChecksFailException"/>.
        /// </summary>
        /// <param name="command">Command that failed to execute.</param>
        /// <param name="context">Context in which the command was executed.</param>
        /// <param name="failedChecks">A collection of checks that failed.</param>
        public VSCommandChecksFailException(Command command, CommandContext context, IEnumerable<VSCommandCheckBaseAttribute> failedChecks)
            : base("One or more pre-execution checks failed.")
        {
            this.Command = command;
            this.Context = context;
            this.FailedChecks = new ReadOnlyCollection<VSCommandCheckBaseAttribute>(new List<VSCommandCheckBaseAttribute>(failedChecks));
        }
    }
}
