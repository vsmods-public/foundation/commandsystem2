namespace CommandSystem2.Exceptions
{
    using System;

    public sealed class VSCommandNotFoundException : Exception
    {
        /// <summary>
        /// Gets the name of the command that was not found.
        /// </summary>
        public string CommandName { get; set; }

        /// <summary>
        /// Creates a new <see cref="VSCommandNotFoundException"/>.
        /// </summary>
        /// <param name="command">Name of the command that was not found.</param>
        public VSCommandNotFoundException(string command)
            : base("Specified command was not found.")
        {
            this.CommandName = command;
        }

        /// <summary>
        /// Returns a string representation of this <see cref="VSCommandNotFoundException"/>.
        /// </summary>
        /// <returns>A string representation.</returns>
        public override string ToString()
        {
            return $"{this.GetType()}: {this.Message}\nCommand name: {this.CommandName}\nDefault exception message:\n" + base.ToString(); // much like System.ArgumentNullException works
        }
    }
}
