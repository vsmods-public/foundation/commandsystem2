namespace CommandSystem2.Exceptions
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Indicates that given argument set already exists as an overload for specified command.
    /// </summary>
    internal class VSCommandDuplicateOverloadException : Exception
    {
        /// <summary>
        /// Gets the name of the command that already has the overload.
        /// </summary>
        private readonly string CommandName;

        /// <summary>
        /// Gets the ordered collection of argument types for the specified overload.
        /// </summary>
        private readonly IReadOnlyList<Type> ArgumentTypes;

        private readonly string ArgumentSetKey;

        /// <summary>
        /// Creates a new exception indicating given argument set already exists as an overload for specified command.
        /// </summary>
        /// <param name="name">Name of the command with duplicated argument sets.</param>
        /// <param name="argumentTypes">Collection of ordered argument types for the command.</param>
        /// <param name="argumentSetKey">Overload identifier.</param>
        internal VSCommandDuplicateOverloadException(string name, List<Type> argumentTypes, string argumentSetKey)
            : base("An overload with specified argument types exists.")
        {
            this.CommandName = name;
            this.ArgumentTypes = argumentTypes;
            this.ArgumentSetKey = argumentSetKey;
        }


        /// <summary>
        /// Returns a string representation of this <see cref="VSCommandDuplicateOverloadException"/>.
        /// </summary>
        /// <returns>A string representation.</returns>
        public override string ToString()
        {
            return $"{this.GetType()}: {this.Message}\nCommand name: {this.CommandName}\nArgument types: {this.ArgumentSetKey}"; // much like System.ArgumentException works
        }
    }
}
