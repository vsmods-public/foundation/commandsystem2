namespace CommandSystem2.Extensions
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;
    using CommandSystem2.Attributes;
    using CommandSystem2.Entities.VSCommand;
    using Microsoft.Extensions.DependencyInjection;

    internal static class ReflectionExtensions
    {
        internal static bool IsModuleCandidateType(this Type type)
        {
            return type.GetTypeInfo().IsModuleCandidateType();
        }

        internal static bool IsModuleCandidateType(this TypeInfo typeInfo)
        {
            // check if compiler-generated
            if (typeInfo.GetCustomAttribute<CompilerGeneratedAttribute>(false) != null)
            {
                return false;
            }

            // check if derives from the required base class
            /*var tmodule = typeof(BaseCommandModule);
            var timodule = tmodule.GetTypeInfo();
            if (!timodule.IsAssignableFrom(ti))
                return false;*/

            // check if anonymous
            if (typeInfo.IsGenericType && typeInfo.Name.Contains("AnonymousType") && (typeInfo.Name.StartsWith("<>") || typeInfo.Name.StartsWith("VB$")) && (typeInfo.Attributes & TypeAttributes.NotPublic) == TypeAttributes.NotPublic)
            {
                return false;
            }

            // check if abstract, static, or not a class
            if (!typeInfo.IsClass || typeInfo.IsAbstract)
            {
                return false;
            }

            // check if delegate type
            var typeDelegate = typeof(Delegate).GetTypeInfo();
            if (typeDelegate.IsAssignableFrom(typeInfo))
            {
                return false;
            }

            // qualifies if any method or type qualifies
            return typeInfo.DeclaredMethods.Any(method => method.IsCommandCandidate(out _)) || typeInfo.DeclaredNestedTypes.Any(type => type.IsModuleCandidateType());
        }

        internal static bool IsCommandCandidate(this MethodInfo method, out ParameterInfo[] parameters)
        {
            parameters = null;
            // check if exists
            if (method == null)
            {
                return false;
            }

            // check if static, non-public, abstract, a constructor, or a special name
            if (method.IsStatic || method.IsAbstract || method.IsConstructor || method.IsSpecialName)
            {
                return false;
            }

            // check if appropriate return and arguments
            parameters = method.GetParameters();
            if (!parameters.Any() || parameters.First().ParameterType != typeof(CommandContext) || method.ReturnType != typeof(Task))
            {
                return false;
            }

            // qualifies
            return true;
        }

        internal static object CreateInstance(this Type t, IServiceProvider services)
        {
            var ti = t.GetTypeInfo();
            var constructors = ti.DeclaredConstructors
                .Where(xci => xci.IsPublic)
                .ToArray();

            if (constructors.Length != 1)
            {
                throw new ArgumentException("Specified type does not contain a public constructor or contains more than one public constructor.");
            }

            var constructor = constructors[0];
            var constructorArgs = constructor.GetParameters();
            var args = new object[constructorArgs.Length];

            if (constructorArgs.Length != 0 && services == null)
            {
                throw new InvalidOperationException("Dependency collection needs to be specified for parametered constructors.");
            }

            // inject via constructor
            if (constructorArgs.Length != 0)
            {
                for (var i = 0; i < args.Length; i++)
                {
                    args[i] = services.GetRequiredService(constructorArgs[i].ParameterType);
                }
            }

            var moduleInstance = Activator.CreateInstance(t, args);

            // inject into properties
            var props = t.GetRuntimeProperties().Where(xp => xp.CanWrite && xp.SetMethod != null && !xp.SetMethod.IsStatic && xp.SetMethod.IsPublic);
            foreach (var prop in props)
            {
                if (prop.GetCustomAttribute<VSCommandDontInjectAttribute>() != null)
                {
                    continue;
                }

                var service = services.GetService(prop.PropertyType);
                if (service == null)
                {
                    continue;
                }

                prop.SetValue(moduleInstance, service);
            }

            // inject into fields
            var fields = t.GetRuntimeFields().Where(xf => !xf.IsInitOnly && !xf.IsStatic && xf.IsPublic);
            foreach (var field in fields)
            {
                if (field.GetCustomAttribute<VSCommandDontInjectAttribute>() != null)
                {
                    continue;
                }

                var service = services.GetService(field.FieldType);
                if (service == null)
                {
                    continue;
                }

                field.SetValue(moduleInstance, service);
            }

            return moduleInstance;
        }
    }
}
