namespace CommandSystem2.Attributes
{
    using System;
    using System.Linq;

    /// <summary>
    /// Marks this method as a command.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class VSCommandAttribute : Attribute
    {
        /// <summary>
        /// Gets the name of this command.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Marks this method as a command, using the method's name as command name.
        /// </summary>
        public VSCommandAttribute()
        {
            this.Name = null;
        }

        /// <summary>
        /// Marks this method as a command with specified name.
        /// </summary>
        /// <param name="name">Name of this command.</param>
        public VSCommandAttribute(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException(nameof(name), "Command names cannot be null, empty, or all-whitespace.");
            }

            if (name.Any(character => char.IsWhiteSpace(character)))
            {
                throw new ArgumentException("Command names cannot contain whitespace characters.", nameof(name));
            }

            this.Name = name;
        }
    }
}
