namespace CommandSystem2.Attributes.CheckAttributes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using CommandSystem2.Entities.VSCommand;
    using Vintagestory.API.Common;

    /// <summary>
    /// Defines that usage of this command is restricted to users with specified permissions.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class VSCommandRequirePermissionAttribute : VSCommandCheckBaseAttribute
    {
        /// <summary>
        /// Gets the name of the role required to execute this command.
        /// </summary>
        public IReadOnlyList<string> PermissionNames { get; }

        /// <summary>
        /// Gets the role checking mode. Refer to <see cref="PermissionCheckMode"/> for more information.
        /// </summary>
        public PermissionCheckMode CheckMode { get; }

        /// <summary>
        /// Defines that usage of this command is restricted to users with specified permissions.
        /// </summary>
        /// <param name="checkMode">Role checking mode.</param>
        /// <param name="roleNames">Names of the role to be verified by this check.</param>
        public VSCommandRequirePermissionAttribute(PermissionCheckMode checkMode, params string[] roleNames)
        {
            this.CheckMode = checkMode;
            this.PermissionNames = new ReadOnlyCollection<string>(roleNames);
        }

        public override Task<bool> ExecuteCheckAsync(CommandContext context, bool help)
        {
            if (context.ServerPlayer == null && context.ClientPlayer == null)
            {
                return Task.FromResult(false);
            }

            IPlayer player = context.ServerAPI == null ? context.ClientPlayer as IPlayer : context.ServerPlayer;
            var privileges = player.Privileges;
            var privilegeCount = privileges.Count();
            var privilegeIntersection = privileges.Intersect(this.PermissionNames, context.CommandSystem.GetStringComparer());
            var privilegeIntersectionCount = privilegeIntersection.Count();

            switch (this.CheckMode)
            {
                case PermissionCheckMode.All:
                    return Task.FromResult(this.PermissionNames.Count == privilegeIntersectionCount);

                case PermissionCheckMode.SpecifiedOnly:
                    return Task.FromResult(privilegeCount == privilegeIntersectionCount);

                case PermissionCheckMode.None:
                    return Task.FromResult(privilegeIntersectionCount == 0);

                case PermissionCheckMode.Any:
                default:
                    return Task.FromResult(privilegeIntersectionCount > 0);
            }
        }
    }

    /// <summary>
    /// Specifies how does <see cref="VSCommandRequirePermissionAttribute"/> check for roles.
    /// </summary>
    public enum PermissionCheckMode
    {
        /// <summary>
        /// Member is required to have any of the specified roles.
        /// </summary>
        Any,

        /// <summary>
        /// Member is required to have all of the specified roles.
        /// </summary>
        All,

        /// <summary>
        /// Member is required to have exactly the same roles as specified; no extra roles may be present.
        /// </summary>
        SpecifiedOnly,

        /// <summary>
        /// Member is required to have none of the specified roles.
        /// </summary>
        None
    }
}
