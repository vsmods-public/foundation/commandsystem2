namespace CommandSystem2.Attributes
{
    using System;

    /// <summary>
    /// Marks this method as a group command.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public sealed class VSCommandGroupCommandAttribute : Attribute
    {
        /// <summary>
        /// Marks this method as a group command.
        /// </summary>
        public VSCommandGroupCommandAttribute()
        {
        }
    }
}
