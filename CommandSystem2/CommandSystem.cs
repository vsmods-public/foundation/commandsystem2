namespace CommandSystem2
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using CommandSystem2.Attributes;
    using CommandSystem2.Attributes.CheckAttributes;
    using CommandSystem2.Entities.Builders;
    using CommandSystem2.Entities.Converters;
    using CommandSystem2.Entities.HelpModule;
    using CommandSystem2.Entities.Interfaces;
    using CommandSystem2.Entities.VSCommand;
    using CommandSystem2.Exceptions;
    using CommandSystem2.Extensions;
    using Vintagestory.API.Client;
    using Vintagestory.API.Common;
    using Vintagestory.API.Config;
    using Vintagestory.API.Server;

    /// <summary>
    /// This is the class which handles command registration, management, and execution. 
    /// </summary>
    public class CommandSystem
    {
        private ICommandSystemConfig Config { get; }
        private MethodInfo ConvertGeneric { get; }
        private ICoreServerAPI ServerAPI { get; }
        private ICoreClientAPI ClientAPI { get; }

        public Dictionary<Type, IArgumentConverter> ArgumentConverters { get; }

        private CommandSystem(ICommandSystemConfig config)
        {
            this.Config = config ?? throw new ArgumentNullException(nameof(config));
            this.topLevelCommands = new Dictionary<string, Command>();
            this.registeredCommandsLazy = new Lazy<IReadOnlyDictionary<string, Command>>(() => new ReadOnlyDictionary<string, Command>(this.topLevelCommands));
            this.ConvertGeneric = this.GetType().GetMethods().First(x => x.IsGenericMethod && x.Name == nameof(ConvertArgumentAsync));

            this.ArgumentConverters = new Dictionary<Type, IArgumentConverter>
            {
                [typeof(string)] = new StringConverter(),
                [typeof(bool)] = new BoolConverter(),
                [typeof(sbyte)] = new Int8Converter(),
                [typeof(byte)] = new Uint8Converter(),
                [typeof(short)] = new Int16Converter(),
                [typeof(ushort)] = new Uint16Converter(),
                [typeof(int)] = new Int32Converter(),
                [typeof(uint)] = new Uint32Converter(),
                [typeof(long)] = new Int64Converter(),
                [typeof(ulong)] = new Uint64Converter(),
                [typeof(float)] = new Float32Converter(),
                [typeof(double)] = new Float64Converter(),
                [typeof(decimal)] = new Float128Converter(),
                [typeof(DateTime)] = new DateTimeConverter(),
                [typeof(DateTimeOffset)] = new DateTimeOffsetConverter(),
                [typeof(TimeSpan)] = new TimeSpanConverter(),
                [typeof(Uri)] = new UriConverter()
            };

            //Add all equivalent Nullable argument converters
            var nullableConverterType = typeof(NullableConverter<>);
            var nullableType = typeof(Nullable<>);
            var argumentConverterTypes = this.ArgumentConverters.Keys.ToArray();
            foreach (var argumentConverter in argumentConverterTypes)
            {
                var typeInfo = argumentConverter.GetTypeInfo();
                if (!typeInfo.IsValueType)
                {
                    continue;
                }

                var genericNullableConverterType = nullableConverterType.MakeGenericType(argumentConverter);
                var genericNullableType = nullableType.MakeGenericType(argumentConverter);
                if (this.ArgumentConverters.ContainsKey(genericNullableConverterType))
                {
                    continue;
                }

                var converterInstance = Activator.CreateInstance(genericNullableConverterType) as IArgumentConverter;
                this.ArgumentConverters[genericNullableType] = converterInstance;
            }
        }

        public CommandSystem(ICommandSystemConfig config, ICoreServerAPI api)
            : this(config)
        {
            this.ServerAPI = api;
#if !DEBUG
            new HelpCommandSystem().StartServerSide(api);
#endif
            this.RegisterCommands(typeof(HelpCommandSystem.HelpCommand));

            if (!api.ObjectCache.ContainsKey("CS2System"))
            {
                api.ObjectCache["CS2System"] = this;
            }
        }

        public CommandSystem(ICommandSystemConfig config, ICoreClientAPI api)
            : this(config)
        {
            this.ClientAPI = api;
#if !DEBUG
            new HelpCommandSystem().StartClientSide(api);
#endif
            this.RegisterCommands(typeof(HelpCommandSystem.HelpCommand));

            if (!api.ObjectCache.ContainsKey("CS2System"))
            {
                api.ObjectCache["CS2System"] = this;
            }
        }

        #region Command Handling
        private void HandleClientCommand(int groupId, CmdArgs arguments, string commandName)
        {
            this.HandleCommand(this.ClientAPI.World.Player, groupId, arguments, commandName).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        private void HandleServerCommand(IServerPlayer player, int groupId, CmdArgs arguments, string commandName)
        {
            this.HandleCommand(player, groupId, arguments, commandName).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        private async Task HandleCommand(IPlayer player, int groupid, CmdArgs arguments, string commandName)
        {
            arguments.PushSingle(commandName);
            var cmd = this.FindCommand(arguments, out var rawArgs);
            var ctx = this.CreateContext(cmd, player, groupid, arguments, this.ServerAPI, this.ClientAPI, rawArgs);
            if (cmd == null)
            {
                throw new VSCommandNotFoundException(commandName);
            }

            _ = Task.Run(async () => await this.ExecuteCommandAsync(ctx));
        }

        /// <summary>
        /// Finds a specified command by its qualified name, then separates arguments.
        /// </summary>
        /// <param name="arguments">The base command system's CmdArgs object to extract arguments from.</param>
        /// <param name="rawArguments">Separated arguments.</param>
        /// <returns>Found command or null if none was found.</returns>
        public Command FindCommand(CmdArgs arguments, out string rawArguments)
        {
            rawArguments = null;

            var next = arguments.PopWord();
            if (next == null)
            {
                return null;
            }

            if (!this.RegisteredCommands.TryGetValue(next, out var cmd))
            {
                next = next.ToLowerInvariant();
                var cmdKvp = this.RegisteredCommands.FirstOrDefault(x => x.Key.ToLowerInvariant() == next);
                if (cmdKvp.Value == null)
                {
                    return null;
                }

                cmd = cmdKvp.Value;
            }

            if (!(cmd is CommandGroup))
            {
                rawArguments = arguments.PopAll();
                return cmd;
            }

            while (cmd is CommandGroup)
            {
                var cm2 = cmd as CommandGroup;
                next = arguments.PopWord();
                if (next == null)
                {
                    break;
                }

                next = next.ToLowerInvariant();
                cmd = cm2.Children.FirstOrDefault(x => x.Name.ToLowerInvariant() == next);

                if (cmd == null)
                {
                    cmd = cm2;
                    break;
                }
            }

            rawArguments = arguments.PopAll();
            return cmd;
        }

        /// <summary>
        /// Creates a command execution context from specified arguments.
        /// </summary>
        /// <param name="command">Command to execute.</param>
        /// <param name="rawArguments">Raw arguments to pass to command.</param>
        /// <returns>Created command execution context.</returns>
        public CommandContext CreateContext(Command command, IPlayer player, int groupId, CmdArgs args, ICoreServerAPI serverApi, ICoreClientAPI clientApi, string rawArguments = null)
        {
            var context = new CommandContext
            {
                Command = command,
                CommandArguments = args,
                Config = this.Config,
                Services = this.Config.Services,
                CommandSystem = this,
                ClientPlayer = player as IClientPlayer,
                ServerPlayer = player as IServerPlayer,
                GroupId = groupId,
                ClientAPI = clientApi,
                ServerAPI = serverApi
            };

            //TODO: figure out why a IServiceProvider given by a mod depending on us, fails to provide any IServiceScopeFactory, but only in a NuGet package
            /*if (command != null)
            {
                var scope = context.Services.CreateScope();
                context.ServiceScopeContext = new CommandContext.ServiceContext(context.Services, scope);
                context.Services = scope.ServiceProvider;
            }*/

            return context;
        }

        /// <summary>
        /// Executes specified command from given context.
        /// </summary>
        /// <param name="context">Context to execute command from.</param>
        /// <returns></returns>
        public async Task ExecuteCommandAsync(CommandContext context)
        {
            try
            {
                var cmd = context.Command;
                await this.RunAllChecksAsync(cmd, context).ConfigureAwait(false);

                var res = await cmd.ExecuteAsync(context).ConfigureAwait(false);
            }
            catch (Exception)
            {
                //TODO: don't fail silently
            }
            finally
            {
                /*if (context.ServiceScopeContext.IsInitialized)
                {
                    context.ServiceScopeContext.Dispose();
                }*/
            }
        }

        private async Task RunAllChecksAsync(Command command, CommandContext context)
        {
            if (command.Parent != null)
            {
                await this.RunAllChecksAsync(command.Parent, context).ConfigureAwait(false);
            }

            var fchecks = await command.RunChecksAsync(context, false).ConfigureAwait(false);
            if (fchecks.Any())
            {
                throw new VSCommandChecksFailException(command, context, fchecks);
            }
        }
        #endregion

        #region Command Registration
        /// <summary>
        /// Gets a dictionary of registered top-level commands.
        /// </summary>
        public IReadOnlyDictionary<string, Command> RegisteredCommands
        {
            get
            {
                var finalDict = this.registeredCommandsLazy.Value;
                if (this.ServerAPI != null && this.ServerAPI.ObjectCache.TryGetValue("CommandSystem2", out var serverDict))
                {
                    finalDict = finalDict.Concat((serverDict as Dictionary<string, Command>).Where(commandKVP => !finalDict.ContainsKey(commandKVP.Key))).ToDictionary(x => x.Key, x => x.Value);
                }
                if (this.ClientAPI != null && this.ClientAPI.ObjectCache.TryGetValue("CommandSystem2", out var clientDict))
                {
                    finalDict = finalDict.Concat((clientDict as Dictionary<string, Command>).Where(commandKVP => !finalDict.ContainsKey(commandKVP.Key))).ToDictionary(x => x.Key, x => x.Value);
                }
                return finalDict;
            }
        }


        private Dictionary<string, Command> topLevelCommands
        {
            get
            {
                var finalDict = new Dictionary<string, Command>();
                if (this.ServerAPI != null)
                {
                    lock (this.ServerAPI.ObjectCache)
                    {
                        if (this.ServerAPI.ObjectCache.TryGetValue("CommandSystem2", out var serverDict))
                        {
                            finalDict = serverDict as Dictionary<string, Command>;
                        }
                        else
                        {
                            this.ServerAPI.ObjectCache["CommandSystem2"] = finalDict;
                        }
                    }
                }
                else if (this.ClientAPI != null)
                {
                    lock (this.ClientAPI.ObjectCache)
                    {
                        if (this.ClientAPI.ObjectCache.TryGetValue("CommandSystem2", out var clientDict))
                        {
                            finalDict = clientDict as Dictionary<string, Command>;
                        }
                        else
                        {
                            this.ClientAPI.ObjectCache["CommandSystem2"] = finalDict;
                        }
                    }
                }

                return finalDict;
            }
            set
            {
                var finalDict = value;
                if (this.ServerAPI != null)
                {
                    lock (this.ServerAPI.ObjectCache)
                    {
                        if (this.ServerAPI.ObjectCache.TryGetValue("CommandSystem2", out var serverDict))
                        {
                            finalDict = finalDict.Concat((serverDict as Dictionary<string, Command>).Where(commandKVP => !finalDict.ContainsKey(commandKVP.Key))).ToDictionary(x => x.Key, x => x.Value);
                        }
                        this.ServerAPI.ObjectCache["CommandSystem2"] = finalDict;
                    }
                }
                else if (this.ClientAPI != null)
                {
                    lock (this.ClientAPI.ObjectCache)
                    {
                        if (this.ClientAPI.ObjectCache.TryGetValue("CommandSystem2", out var clientDict))
                        {
                            finalDict = finalDict.Concat((clientDict as Dictionary<string, Command>).Where(commandKVP => !finalDict.ContainsKey(commandKVP.Key))).ToDictionary(x => x.Key, x => x.Value);
                        }
                        this.ClientAPI.ObjectCache["CommandSystem2"] = finalDict;
                    }
                }
            }
        }
        private readonly Lazy<IReadOnlyDictionary<string, Command>> registeredCommandsLazy;

        /// <summary>
        /// Registers all commands from a given assembly. The command classes need to be public to be considered for registration.
        /// </summary>
        /// <param name="assembly">Assembly to register commands from.</param>
        public void RegisterCommands(Assembly assembly)
        {
            var types = assembly.ExportedTypes.Where(x => x.IsModuleCandidateType() && !x.IsNested);
            foreach (var type in types)
            {
                this.RegisterCommands(type);
            }
        }

        /// <summary>
        /// Registers all commands from a given command class. Remember that all methods must return a Task.
        /// </summary>
        /// <param name="type">Type of the class which holds commands to register.</param>
        public void RegisterCommands(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type), "Type cannot be null.");
            }

            if (!type.IsModuleCandidateType())
            {
                throw new ArgumentNullException(nameof(type), "Type must be a class, which cannot be abstract or static. Command handler functions must return Task.");
            }

            if (type == typeof(HelpCommandSystem.HelpCommand) && this.topLevelCommands.ContainsKey("exthelp"))
            {
                return;
            }

            var tempCommands = this.RegisterCommands(type, null);
            if (tempCommands != null)
            {
                foreach (var command in tempCommands)
                {
                    this.AddToCommandDictionary(command.Build(null));
                }
            }
        }

        /// <summary>
        /// Builds and registers all supplied commands.
        /// </summary>
        /// <param name="commands">Commands to build and register.</param>
        public void RegisterCommands(params CommandBuilder[] commands)
        {
            foreach (var command in commands)
            {
                this.AddToCommandDictionary(command.Build(null));
            }
        }

        private List<CommandBuilder> RegisterCommands(Type type, CommandGroupBuilder currentParent)
        {
            var typeInfo = type.GetTypeInfo();
            var moduleAttributes = typeInfo.GetCustomAttributes();

            var groupBuilder = new CommandGroupBuilder();
            var isModule = false;
            var moduleChecks = new List<VSCommandCheckBaseAttribute>();
            foreach (var attrib in moduleAttributes)
            {
                switch (attrib)
                {
                    case VSCommandGroupAttribute group:
                        isModule = true;
                        var moduleName = group.Name;
                        if (moduleName == null)
                        {
                            moduleName = typeInfo.Name;

                            if (moduleName.EndsWith("Group") && moduleName != "Group")
                            {
                                moduleName = moduleName.Substring(0, moduleName.Length - 5);
                            }
                            else if (moduleName.EndsWith("Module") && moduleName != "Module")
                            {
                                moduleName = moduleName.Substring(0, moduleName.Length - 6);
                            }
                            else if (moduleName.EndsWith("Commands") && moduleName != "Commands")
                            {
                                moduleName = moduleName.Substring(0, moduleName.Length - 8);
                            }
                        }

                        moduleName = moduleName.ToLowerInvariant();

                        groupBuilder.WithName(moduleName);

                        foreach (var method in typeInfo.DeclaredMethods.Where(x => x.IsCommandCandidate(out _) && x.GetCustomAttribute<VSCommandGroupCommandAttribute>() != null))
                        {
                            groupBuilder.WithOverload(new CommandOverloadBuilder(method));
                        }

                        break;

                    case VSCommandDescriptionAttribute description:
                        groupBuilder.WithDescription(description.Description);
                        break;

                    case VSCommandCheckBaseAttribute check:
                        moduleChecks.Add(check);
                        groupBuilder.WithExecutionCheck(check);
                        break;

                    default:
                        break;
                }
            }

            if (!isModule)
            {
                groupBuilder = null;
            }

            // candidate methods
            var methods = typeInfo.DeclaredMethods;
            var commands = new List<CommandBuilder>();
            var commandBuilders = new Dictionary<string, CommandBuilder>();
            foreach (var method in methods)
            {
                if (!method.IsCommandCandidate(out _))
                {
                    continue;
                }

                var attributes = method.GetCustomAttributes();
                //Go through all the attributes and find the one that is the first VSCommandAttribute
                if (!(attributes.FirstOrDefault(attrib => attrib is VSCommandAttribute) is VSCommandAttribute commandAttribute))
                {
                    continue;
                }

                var commandName = commandAttribute.Name;
                if (commandName == null)
                {
                    commandName = method.Name;
                    if (commandName.EndsWith("Async") && commandName != "Async")
                    {
                        commandName = commandName.Substring(0, commandName.Length - 5);
                    }
                }

                commandName = commandName.ToLowerInvariant();

                if (!commandBuilders.TryGetValue(commandName, out var commandBuilder))
                {
                    commandBuilders.Add(commandName, commandBuilder = new CommandBuilder().WithName(commandName));

                    if (!isModule)
                    {
                        if (currentParent != null)
                        {
                            currentParent.WithChild(commandBuilder);
                        }
                        else
                        {
                            commands.Add(commandBuilder);
                        }
                    }
                    else
                    {
                        groupBuilder.WithChild(commandBuilder);
                    }
                }

                commandBuilder.WithOverload(new CommandOverloadBuilder(method));

                if (!isModule && moduleChecks.Any())
                {
                    foreach (var chk in moduleChecks)
                    {
                        commandBuilder.WithExecutionCheck(chk);
                    }
                }

                foreach (var attrib in attributes)
                {
                    switch (attrib)
                    {
                        case VSCommandDescriptionAttribute description:
                            commandBuilder.WithDescription(description.Description);
                            break;

                        case VSCommandCheckBaseAttribute check:
                            commandBuilder.WithExecutionCheck(check);
                            break;

                        default:
                            break;
                    }
                }
            }

            // candidate types
            var types = typeInfo.DeclaredNestedTypes
                .Where(xt => xt.IsModuleCandidateType() && xt.DeclaredConstructors.Any(xc => xc.IsPublic));
            foreach (var candidateType in types)
            {
                var tempCommands = this.RegisterCommands(candidateType.AsType(), groupBuilder);

                if (isModule && tempCommands != null)
                {
                    foreach (var xtcmd in tempCommands)
                    {
                        groupBuilder.WithChild(xtcmd);
                    }
                }
                else if (tempCommands != null)
                {
                    commands.AddRange(tempCommands);
                }
            }

            if (isModule && currentParent == null)
            {
                commands.Add(groupBuilder);
            }
            else if (isModule)
            {
                currentParent.WithChild(groupBuilder);
            }

            return commands;
        }

        /// <summary>
        /// Unregisters specified commands from the <see cref="CommandSystem"/>.
        /// </summary>
        /// <param name="commands">Commands to unregister.</param>
        public void UnregisterCommands(params Command[] commands)
        {
            if (commands.Any(x => x.Parent != null))
            {
                throw new InvalidOperationException("Cannot unregister nested commands.");
            }

            var keys = this.RegisteredCommands.Where(x => commands.Contains(x.Value)).Select(x => x.Key).ToList();
            foreach (var key in keys)
            {
                this.topLevelCommands.Remove(key);
            }
        }

        private void AddToCommandDictionary(Command command)
        {
            if (command.Parent != null)
            {
                return;
            }

            if (this.topLevelCommands.ContainsKey(command.Name))
            {
                throw new VSCommandDuplicateCommandException(command.QualifiedName);
            }

            this.topLevelCommands[command.Name] = command;
            if (this.ServerAPI != null)
            {
                this.ServerAPI.RegisterCommand(command.Name, command.Description, "", (p, g, a) => this.HandleServerCommand(p, g, a, command.Name));
            }
            else
            {
                this.ClientAPI.RegisterCommand(command.Name, command.Description, "", (g, a) => this.HandleClientCommand(g, a, command.Name));
            }
        }
        #endregion

        internal async Task<ArgumentBindingResult> BindArguments(CommandContext context, bool ignoreExtraArgs = false)
        {
            var command = context.Command;
            var overload = context.Overload;

            if (context.CommandArguments.Length > overload.Arguments.Count && !ignoreExtraArgs && !overload.Arguments.Any(x => x.IsCatchAll))
            {
                return new ArgumentBindingResult(new ArgumentException("Too many arguments were supplied to this command."));
            }

            var arguments = new object[overload.Arguments.Count + 2];
            arguments[1] = context;

            var rawArgString = context.CommandArguments.Clone().PopAll().Split(' ').Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
            for (var i = 0; i < overload.Arguments.Count; i++)
            {
                var argument = overload.Arguments[i];
                if (i > context.CommandArguments.Length && !argument.IsOptional && !argument.IsCatchAll)
                {
                    return new ArgumentBindingResult(new ArgumentException("Not enough arguments supplied to the command."));
                }

                if (argument.IsCatchAll && argument.IsArray)
                {
                    var array = Array.CreateInstance(argument.Type, rawArgString.Length - i);
                    var start = i;
                    while (i < rawArgString.Length)
                    {
                        try
                        {
                            array.SetValue(await this.ConvertArgumentAsync(rawArgString[i], context, argument.Type).ConfigureAwait(false), i - start);
                        }
                        catch (Exception ex)
                        {
                            return new ArgumentBindingResult(ex);
                        }
                        i++;
                    }

                    arguments[start + 2] = array;
                    break;
                }
                else
                {
                    try
                    {
                        var rawArgument = context.CommandArguments.PopWord();
                        if (rawArgument == null)
                        {
                            arguments[i + 2] = argument.DefaultValue;
                        }
                        else
                        {
                            var convertedValue = await this.ConvertArgumentAsync(rawArgument, context, argument.Type).ConfigureAwait(false);
                            arguments[i + 2] = convertedValue ?? argument.DefaultValue;
                        }
                    }
                    catch (Exception ex)
                    {
                        return new ArgumentBindingResult(ex);
                    }
                }
            }

            return new ArgumentBindingResult(arguments);
        }

        #region Type converters
        /// <summary>
        /// Converts a string to specified type.
        /// </summary>
        /// <typeparam name="T">Type to convert to.</typeparam>
        /// <param name="value">Value to convert.</param>
        /// <param name="context">Context in which to convert to.</param>
        /// <returns>Converted object.</returns>
        public async Task<object> ConvertArgumentAsync<T>(string value, CommandContext context)
        {
            var type = typeof(T);
            if (!this.ArgumentConverters.ContainsKey(type))
            {
                throw new ArgumentException("There is no converter specified for given type.", nameof(T));
            }

            if (!(this.ArgumentConverters[type] is IArgumentConverter<T> converter))
            {
                throw new ArgumentException("Invalid converter registered for this type.", nameof(T));
            }

            var cvr = await converter.ConvertAsync(value, context).ConfigureAwait(false);
            if (!cvr.HasValue)
            {
                throw new ArgumentException("Could not convert specified value to given type.", nameof(value));
            }

            return cvr.Value;
        }

        /// <summary>
        /// Converts a string to specified type.
        /// </summary>
        /// <param name="value">Value to convert.</param>
        /// <param name="context">Context in which to convert to.</param>
        /// <param name="type">Type to convert to.</param>
        /// <returns>Converted object.</returns>
        public async Task<object> ConvertArgumentAsync(string value, CommandContext context, Type type)
        {
            var genericMethod = this.ConvertGeneric.MakeGenericMethod(type);
            try
            {
                return await (genericMethod.Invoke(this, new object[] { value, context }) as Task<object>).ConfigureAwait(false);
            }
            catch (TargetInvocationException ex)
            {
                throw ex.InnerException;
            }
        }

        /// <summary>
        /// Registers an argument converter for specified type.
        /// </summary>
        /// <typeparam name="T">Type for which to register the converter.</typeparam>
        /// <param name="converter">Converter to register.</param>
        public void RegisterConverter<T>(IArgumentConverter<T> converter)
        {
            if (converter == null)
            {
                throw new ArgumentNullException(nameof(converter), "Converter cannot be null.");
            }

            var type = typeof(T);
            var typeInfo = type.GetTypeInfo();
            this.ArgumentConverters[type] = converter;

            if (!typeInfo.IsValueType)
            {
                return;
            }

            var nullableConverterType = typeof(NullableConverter<>).MakeGenericType(type);
            var nullableType = typeof(Nullable<>).MakeGenericType(type);
            if (this.ArgumentConverters.ContainsKey(nullableType))
            {
                return;
            }

            var nullableConverter = Activator.CreateInstance(nullableConverterType) as IArgumentConverter;
            this.ArgumentConverters[nullableType] = nullableConverter;
        }

        /// <summary>
        /// Unregisters an argument converter for specified type.
        /// </summary>
        /// <typeparam name="T">Type for which to unregister the converter.</typeparam>
        public void UnregisterConverter<T>()
        {
            var type = typeof(T);
            var typeInfo = type.GetTypeInfo();
            if (this.ArgumentConverters.ContainsKey(type))
            {
                this.ArgumentConverters.Remove(type);
            }

            if (!typeInfo.IsValueType)
            {
                return;
            }

            var nullableType = typeof(Nullable<>).MakeGenericType(type);
            if (!this.ArgumentConverters.ContainsKey(nullableType))
            {
                return;
            }

            this.ArgumentConverters.Remove(nullableType);
        }
        #endregion

        #region Helpers
        /// <summary>
        /// Gets the string comparer. This returns <see cref="StringComparer.OrdinalIgnoreCase"/> currently, but allows for easy separation in the future, if needed.
        /// </summary>
        /// <returns>A string comparer.</returns>
        internal IEqualityComparer<string> GetStringComparer()
        {
            return StringComparer.Ordinal;
        }

        public CommandContext CreateFakeContext(IPlayer player, string commandName, params string[] arguments)
        {
            var config = new HelpConfig();

            var context = new CommandContext()
            {
                Command = this.topLevelCommands[commandName],
                CommandArguments = new CmdArgs(arguments),
                CommandSystem = this,
                Config = config,
                GroupId = GlobalConstants.AllChatGroups,
                Overload = this.topLevelCommands[commandName].Overloads.First(),
                ClientPlayer = player as IClientPlayer,
                ServerPlayer = player as IServerPlayer,
                Services = config.Services,
                ClientAPI = this.ClientAPI,
                ServerAPI = this.ServerAPI
            };

            return context;
        }
        #endregion

        #region HelpModule
        public async Task<Dictionary<string, string>> RenderCommandsAsString(CommandContext context, params string[] commandKeywords)
        {
            var topLevel = context.CommandSystem.topLevelCommands.Values.Distinct();
            var finalDict = new Dictionary<string, string>();

            if (commandKeywords != null && commandKeywords.Any())
            {
                Command command = null;
                var searchIn = topLevel;
                foreach (var commandKeyword in commandKeywords)
                {
                    if (searchIn == null)
                    {
                        command = null;
                        break;
                    }

                    command = searchIn.FirstOrDefault(xc => xc.Name.ToLowerInvariant() == commandKeyword.ToLowerInvariant());

                    if (command == null)
                    {
                        break;
                    }

                    var failedChecks = await command.RunChecksAsync(context, true).ConfigureAwait(false);
                    if (failedChecks.Any())
                    {
                        throw new VSCommandChecksFailException(command, context, failedChecks);
                    }

                    if (command is CommandGroup)
                    {
                        searchIn = (command as CommandGroup).Children;
                    }
                    else
                    {
                        searchIn = null;
                    }
                }

                if (command == null)
                {
                    throw new VSCommandNotFoundException(string.Join(" ", commandKeywords));
                }

                if (command is CommandGroup group)
                {
                    var commandsToSearch = group.Children;
                    var eligibleCommands = new List<Command>();
                    foreach (var candidateCommand in commandsToSearch)
                    {
                        if (candidateCommand.ExecutionChecks == null || !candidateCommand.ExecutionChecks.Any())
                        {
                            eligibleCommands.Add(candidateCommand);
                            continue;
                        }

                        var candidateFailedChecks = await candidateCommand.RunChecksAsync(context, true).ConfigureAwait(false);
                        if (!candidateFailedChecks.Any())
                        {
                            eligibleCommands.Add(candidateCommand);
                        }
                    }

                    if (eligibleCommands.Any())
                    {
                        finalDict.Add(command.Name, await this.RenderCommandDetailsAsString(context, command, eligibleCommands.OrderBy(x => x.Name).ToArray()).ConfigureAwait(false));
                    }
                    else
                    {
                        finalDict.Add(command.Name, await this.RenderCommandDetailsAsString(context, command).ConfigureAwait(false));
                    }
                }
                else
                {
                    var candidateFailedChecks = await command.RunChecksAsync(context, true).ConfigureAwait(false);
                    if (!candidateFailedChecks.Any())
                    {
                        finalDict.Add(command.Name, await this.RenderCommandDetailsAsString(context, command).ConfigureAwait(false));
                    }
                    else
                    {
                        finalDict.Add("", "No valid commands found.");
                    }
                }
            }
            else
            {
                var commandsToSearch = topLevel;
                var eligibleCommands = new List<Command>();

                foreach (var command in commandsToSearch)
                {
                    if (command.ExecutionChecks == null || !command.ExecutionChecks.Any())
                    {
                        eligibleCommands.Add(command);
                        continue;
                    }

                    var commandFailedChecks = await command.RunChecksAsync(context, true).ConfigureAwait(false);
                    if (!commandFailedChecks.Any())
                    {
                        eligibleCommands.Add(command);
                    }
                }

                foreach (var command in eligibleCommands)
                {
                    finalDict.Add(command.Name, await this.RenderCommandAsString(context, command).ConfigureAwait(false));
                }
            }

            return finalDict;
        }

        public async Task<string> RenderCommandAsString(CommandContext context, Command command, params Command[] subCommands)
        {
            return $"{(this.ServerAPI != null ? "/" : ".")}{command.Name}: {command.Description ?? "No description provided."}";
        }

        public async Task<string> RenderCommandDetailsAsString(CommandContext context, Command command, params Command[] subCommands)
        {
            var sb = new StringBuilder();

            sb.AppendLine($"{command.Name}: {command.Description ?? "No description provided."}");
            sb.AppendLine();

            if (command is CommandGroup commandGroup && commandGroup.IsExecutableWithoutSubcommands)
            {
                sb.AppendLine("This group can be executed as a standalone command.");
                sb.AppendLine();
            }

            if (command.Overloads?.Any() ?? false)
            {
                var overloads = command.Overloads.OrderBy(x => x.Arguments.Count);
                foreach (var overload in overloads)
                {
                    var overloadStringBuilder = new StringBuilder();
                    var argumentStringBuilder = new StringBuilder();
                    overloadStringBuilder.Append($"Syntax: {(context.ServerAPI == null ? "." : "/")}{command.QualifiedName}");
                    foreach (var argument in overload.Arguments)
                    {
                        overloadStringBuilder.Append(" ");
                        if (argument.IsOptional)
                        {
                            overloadStringBuilder.Append($"[{argument.Name}]");
                        }
                        else
                        {
                            overloadStringBuilder.Append($"<{argument.Name}>");
                        }

                        argumentStringBuilder.AppendLine($"- {argument.Name} ({argument.Type.Name.ToLowerInvariant()}): {argument.Description ?? "No description provided."}");
                    }

                    sb.AppendLine(overloadStringBuilder.ToString());
                    if (overload.Arguments.Count > 0)
                        sb.AppendLine(argumentStringBuilder.ToString());
                }
            }

            return sb.ToString();
        }

        #endregion
    }
}
