namespace CommandSystem2.Entities.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text;
    using CommandSystem2.Attributes;
    using CommandSystem2.Entities.VSCommand;
    using CommandSystem2.Exceptions;
    using CommandSystem2.Extensions;

    public class CommandOverloadBuilder
    {
        /// <summary>
        /// Gets a value that uniquely identifies an overload.
        /// </summary>
        internal string ArgumentSet { get; }

        /// <summary>
        /// Gets the collection of arguments this overload takes.
        /// </summary>
        public IReadOnlyList<CommandArgument> Arguments { get; }

        /// <summary>
        /// Gets the overload's callable delegate.
        /// </summary>
        public Delegate Callable { get; set; }

        private object InvocationTarget { get; }
        private Type InvocationType { get; }

        /// <summary>
        /// Creates a new command overload builder from specified method.
        /// </summary>
        /// <param name="method">Method to use for this overload.</param>
        public CommandOverloadBuilder(MethodInfo method)
            : this(method, null)
        { }

        /// <summary>
        /// Creates a new command overload builder from specified delegate.
        /// </summary>
        /// <param name="method">Delegate to use for this overload.</param>
        public CommandOverloadBuilder(Delegate method)
            : this(method.GetMethodInfo(), method.Target)
        { }

        private CommandOverloadBuilder(MethodInfo method, object target)
        {
            if (!method.IsCommandCandidate(out var parameters))
            {
                throw new ArgumentException("Specified method is not suitable for a command.", nameof(method));
            }

            this.InvocationTarget = target;
            this.InvocationType = method.DeclaringType;

            // create the argument array
            var expression = new ParameterExpression[parameters.Length + 1];
            var iep = Expression.Parameter(target?.GetType() ?? method.DeclaringType, "instance");
            expression[0] = iep;
            expression[1] = Expression.Parameter(typeof(CommandContext), "ctx");

            var i = 2;
            var args = new List<CommandArgument>(parameters.Length - 1);
            var setb = new StringBuilder();
            foreach (var arg in parameters.Skip(1))
            {
                setb.Append(arg.ParameterType).Append(";");
                var commandArgument = new CommandArgument
                {
                    Name = arg.Name,
                    Type = arg.ParameterType,
                    IsOptional = arg.IsOptional,
                    DefaultValue = arg.IsOptional ? arg.DefaultValue : null
                };

                var attributes = arg.GetCustomAttributes();
                var isParams = false;
                foreach (var attrib in attributes)
                {
                    switch (attrib)
                    {
                        case VSCommandDescriptionAttribute descriptionAttribute:
                            commandArgument.Description = descriptionAttribute.Description;
                            break;

                        case VSCommandRemainingTextAttribute remainingTextAttribute:
                            commandArgument.IsCatchAll = true;
                            break;

                        case ParamArrayAttribute p:
                            commandArgument.IsCatchAll = true;
                            commandArgument.Type = arg.ParameterType.GetElementType();
                            commandArgument.IsArray = true;
                            isParams = true;
                            break;
                    }
                }

                if (i > 2 && !commandArgument.IsOptional && !commandArgument.IsCatchAll && args[i - 3].IsOptional)
                {
                    throw new VSCommandInvalidOverloadException("Non-optional argument cannot appear after an optional one", method, arg);
                }

                if (arg.ParameterType.IsArray && !isParams)
                {
                    throw new VSCommandInvalidOverloadException("Cannot use array arguments without params modifier.", method, arg);
                }

                args.Add(commandArgument);
                expression[i++] = Expression.Parameter(arg.ParameterType, arg.Name);
            }

            var expressionCall = Expression.Call(iep, method, expression.Skip(1));
            var expressionLambda = Expression.Lambda(expressionCall, expression);

            this.ArgumentSet = setb.ToString();
            this.Arguments = new ReadOnlyCollection<CommandArgument>(args);
            this.Callable = expressionLambda.Compile();
        }

        internal CommandOverload Build()
        {
            var overload = new CommandOverload()
            {
                Arguments = this.Arguments,
                Callable = this.Callable,
                InvocationTarget = this.InvocationTarget,
                InvocationType = this.InvocationType
            };

            return overload;
        }
    }
}
