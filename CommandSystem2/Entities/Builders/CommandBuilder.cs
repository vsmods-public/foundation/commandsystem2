namespace CommandSystem2.Entities.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using CommandSystem2.Attributes.CheckAttributes;
    using CommandSystem2.Entities.VSCommand;
    using CommandSystem2.Exceptions;

    /// <summary>
    /// Represents an interface to build a command.
    /// </summary>
    public class CommandBuilder
    {
        /// <summary>
        /// Gets the name set for this command.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the description set for this command.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the execution checks defined for this command.
        /// </summary>
        public IReadOnlyList<VSCommandCheckBaseAttribute> ExecutionChecks { get; }
        private List<VSCommandCheckBaseAttribute> ExecutionCheckList { get; }

        /// <summary>
        /// Gets the collection of this command's overloads.
        /// </summary>
        public IReadOnlyList<CommandOverloadBuilder> Overloads { get; }
        private List<CommandOverloadBuilder> OverloadList { get; }
        private HashSet<string> OverloadArgumentSets { get; }

        public CommandBuilder()
        {
            this.ExecutionCheckList = new List<VSCommandCheckBaseAttribute>();
            this.ExecutionChecks = new ReadOnlyCollection<VSCommandCheckBaseAttribute>(this.ExecutionCheckList);

            this.OverloadArgumentSets = new HashSet<string>();
            this.OverloadList = new List<CommandOverloadBuilder>();
            this.Overloads = new ReadOnlyCollection<CommandOverloadBuilder>(this.OverloadList);
        }

        /// <summary>
        /// Sets the name for this command.
        /// </summary>
        /// <param name="name">Name for this command.</param>
        /// <returns>This builder.</returns>
        public CommandBuilder WithName(string name)
        {
            if (name == null || name.ToCharArray().Any(character => char.IsWhiteSpace(character)))
            {
                throw new ArgumentException("Command name cannot be null or contain any whitespace characters.", nameof(name));
            }

            if (this.Name != null)
            {
                throw new InvalidOperationException("This command already has a name.");
            }

            this.Name = name;
            return this;
        }

        /// <summary>
        /// Sets the description for this command.
        /// </summary>
        /// <param name="description">Description to use for this command.</param>
        /// <returns>This builder.</returns>
        public CommandBuilder WithDescription(string description)
        {
            this.Description = description;
            return this;
        }

        /// <summary>
        /// Adds pre-execution checks to this command.
        /// </summary>
        /// <param name="checks">Pre-execution checks to add to this command.</param>
        /// <returns>This builder.</returns>
        public CommandBuilder WithExecutionChecks(params VSCommandCheckBaseAttribute[] checks)
        {
            this.ExecutionCheckList.AddRange(checks.Except(this.ExecutionCheckList));
            return this;
        }

        /// <summary>
        /// Adds a pre-execution check to this command.
        /// </summary>
        /// <param name="check">Pre-execution check to add to this command.</param>
        /// <returns>This builder.</returns>
        public CommandBuilder WithExecutionCheck(VSCommandCheckBaseAttribute check)
        {
            if (!this.ExecutionCheckList.Contains(check))
            {
                this.ExecutionCheckList.Add(check);
            }

            return this;
        }

        /// <summary>
        /// Adds overloads to this command. An executable command needs to have at least one overload.
        /// </summary>
        /// <param name="overloads">Overloads to add to this command.</param>
        /// <returns>This builder.</returns>
        public CommandBuilder WithOverloads(params CommandOverloadBuilder[] overloads)
        {
            foreach (var overload in overloads)
            {
                this.WithOverload(overload);
            }

            return this;
        }

        /// <summary>
        /// Adds an overload to this command. An executable command needs to have at least one overload.
        /// </summary>
        /// <param name="overload">Overload to add to this command.</param>
        /// <returns>This builder.</returns>
        public CommandBuilder WithOverload(CommandOverloadBuilder overload)
        {
            if (this.OverloadArgumentSets.Contains(overload.ArgumentSet))
            {
                throw new VSCommandDuplicateOverloadException(this.Name, overload.Arguments.Select(x => x.Type).ToList(), overload.ArgumentSet);
            }

            this.OverloadArgumentSets.Add(overload.ArgumentSet);
            this.OverloadList.Add(overload);

            return this;
        }

        internal virtual Command Build(CommandGroup parent)
        {
            var cmd = new Command
            {
                Name = this.Name,
                Description = this.Description,
                ExecutionChecks = this.ExecutionChecks,
                Parent = parent,
                Overloads = new ReadOnlyCollection<CommandOverload>(this.Overloads.Select(overload => overload.Build()).ToList()),
            };

            return cmd;
        }
    }
}
