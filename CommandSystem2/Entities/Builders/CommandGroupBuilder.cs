namespace CommandSystem2.Entities.Builders
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using CommandSystem2.Entities.VSCommand;

    /// <summary>
    /// Represents an interface to build a command group.
    /// </summary>
    public sealed class CommandGroupBuilder : CommandBuilder
    {
        /// <summary>
        /// Gets the list of child commands registered for this group.
        /// </summary>
        public IReadOnlyList<CommandBuilder> Children { get; }
        private List<CommandBuilder> ChildrenList { get; }

        /// <summary>
        /// Creates a new module-less command group builder.
        /// </summary>
        public CommandGroupBuilder()
        {
            this.ChildrenList = new List<CommandBuilder>();
            this.Children = new ReadOnlyCollection<CommandBuilder>(this.ChildrenList);
        }

        /// <summary>
        /// Adds a command to the collection of child commands for this group.
        /// </summary>
        /// <param name="child">Command to add to the collection of child commands for this group.</param>
        /// <returns>This builder.</returns>
        public CommandGroupBuilder WithChild(CommandBuilder child)
        {
            this.ChildrenList.Add(child);
            return this;
        }

        internal override Command Build(CommandGroup parent)
        {
            var command = new CommandGroup
            {
                Name = this.Name,
                Description = this.Description,
                ExecutionChecks = this.ExecutionChecks,
                Parent = parent,
                Overloads = new ReadOnlyCollection<CommandOverload>(this.Overloads.Select(overload => overload.Build()).ToList()),
            };

            var commands = new List<Command>();
            foreach (var commandBuilder in this.Children)
            {
                commands.Add(commandBuilder.Build(command));
            }

            command.Children = new ReadOnlyCollection<Command>(commands);
            return command;
        }
    }
}
