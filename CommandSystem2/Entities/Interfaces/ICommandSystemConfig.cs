namespace CommandSystem2.Entities.Interfaces
{
    using System;

    public interface ICommandSystemConfig
    {
        IServiceProvider Services { get; set; }
        bool IgnoreExtraArguments { get; }
    }
}
