namespace CommandSystem2.Entities.Interfaces
{
    using System.Threading.Tasks;
    using CommandSystem2.Entities;
    using CommandSystem2.Entities.VSCommand;

    /// <summary>
    /// Argument converter abstract.
    /// </summary>
    public interface IArgumentConverter
    {

    }

    /// <summary>
    /// Represents a converter for specific argument type.
    /// </summary>
    /// <typeparam name="T">Type for which the converter is to be registered.</typeparam>
    public interface IArgumentConverter<T> : IArgumentConverter
    {
        Task<Optional<T>> ConvertAsync(string value, CommandContext context);
    }
}
