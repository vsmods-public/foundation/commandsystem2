namespace CommandSystem2.Entities.Interfaces
{
    // used internally to make serialization more convenient, do NOT change this, do NOT implement this yourself
    internal interface IOptional
    {
        bool HasValue { get; }
        object RawValue { get; } // must NOT throw InvalidOperationException
    }
}
