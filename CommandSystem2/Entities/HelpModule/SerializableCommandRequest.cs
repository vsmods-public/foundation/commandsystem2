namespace CommandSystem2.Entities.HelpModule
{
    using ProtoBuf;

    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class SerializableCommandRequest
    {
        public string[] Commands { get; set; }
    }
}
