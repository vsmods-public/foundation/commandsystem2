namespace CommandSystem2.Entities.HelpModule
{
    using System;
    using CommandSystem2.Entities.Interfaces;
    using Microsoft.Extensions.DependencyInjection;

    public class HelpConfig : ICommandSystemConfig
    {
        public IServiceProvider Services { get; set; } = new ServiceCollection().BuildServiceProvider(true);

        public bool IgnoreExtraArguments => true;
    }
}
