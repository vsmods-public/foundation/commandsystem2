namespace CommandSystem2.Entities.HelpModule
{
    using System.Collections.Generic;
    using ProtoBuf;

    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class SerializableCommandResponse
    {
        public Dictionary<string, string> RenderedCommandStrings { get; set; }
        public string RenderedCommandStringDetails { get; set; }
        public string[] RequestedCommand { get; internal set; }
    }
}
