namespace CommandSystem2.Entities.HelpModule
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using CommandSystem2.Attributes;
    using CommandSystem2.Entities.VSCommand;
    using CommandSystem2.Exceptions;
    using Vintagestory.API.Client;
    using Vintagestory.API.Common;
    using Vintagestory.API.Config;
    using Vintagestory.API.Server;

#if DEBUG
    public class HelpCommandSystem : ModSystem
#else
    public class HelpCommandSystem
#endif
    {
        private const string CHANNELNAME = "CommandSystem2HelpChannel";
#if DEBUG
        private CommandSystem debugCommandSystem;
#endif

        public class HelpCommand
        {
            [VSCommand("exthelp"), VSCommandDescription("Displays help for all registered commands.")]
            public async Task DefaultHelpAsync(CommandContext context, [VSCommandDescription("The command to provide help for.")] params string[] command)
            {
                if (context.ServerAPI != null && IsClientLoaded)
                {
                    context.ServerPlayer.SendMessage(context.GroupId, "Use the clientside command of .exthelp instead.", EnumChatType.CommandSuccess);
                    return;
                }

                ClientChannel.SendPacket(new SerializableCommandRequest() { Commands = command });
            }
        }

        #region Client
        private ICoreClientAPI clientApi;
        protected static IClientNetworkChannel ClientChannel { get; private set; }
#if DEBUG
        public override void StartClientSide(ICoreClientAPI api)
#else
        public void StartClientSide(ICoreClientAPI api)
#endif
        {
            this.clientApi = api;
            ClientChannel =
                api.Network.RegisterChannel(CHANNELNAME)
                .RegisterMessageType<ClientLoadedRequest>()
                .SetMessageHandler<ClientLoadedRequest>(this.OnServerReady)
                .RegisterMessageType<SerializableCommandRequest>()
                .RegisterMessageType<SerializableCommandResponse>()
                .SetMessageHandler<SerializableCommandResponse>(this.OnServerHelpResponse);

#if DEBUG
            this.debugCommandSystem = new CommandSystem(new HelpConfig(), api);
#endif

            api.Event.IsPlayerReady += this.Event_IsPlayerReady;
        }

        private bool Event_IsPlayerReady(ref EnumHandling handling)
        {
            if (ClientChannel.Connected)
            {
                ClientChannel.SendPacket(new ClientLoadedRequest());
            }

            return true;
        }

        private void OnServerReady(ClientLoadedRequest networkMessage)
        {
            ClientChannel.SendPacket(new ClientLoadedRequest());
        }

        private void OnServerHelpResponse(SerializableCommandResponse networkMessage)
        {
            var cs2 = this.clientApi.ObjectCache["CS2System"] as CommandSystem;
            var player = this.clientApi.World.Player;
            Dictionary<string, string> clientCommands = null;
            try
            {
                clientCommands = cs2.RenderCommandsAsString(cs2.CreateFakeContext(player, "exthelp"), networkMessage.RequestedCommand).ConfigureAwait(false).GetAwaiter().GetResult();
            }
            catch (VSCommandNotFoundException)
            {
                clientCommands = new Dictionary<string, string>();
            }
            catch (Exception)
            {
                player.ShowChatNotification("An unknown error occurred.");
                return;
            }

            var totalCommands = clientCommands.Concat(networkMessage.RenderedCommandStrings.Where(x => x.Key != "exthelp").OrderBy(x => x.Key));
            if (totalCommands.Count() == 1 && networkMessage.RequestedCommand.Length > 0)
            {
                player.ShowChatNotification("Detailed command usage for " + string.Join(" ", networkMessage.RequestedCommand));
            }
            else
            {
                player.ShowChatNotification("All commands found on both the server and client:");
            }
            foreach (var description in totalCommands)
            {
                player.ShowChatNotification(description.Value.Replace("<", "&lt;").Replace(">", "&gt;").Trim());
            }
        }
        #endregion

        #region Server
        private ICoreServerAPI serverApi;
        protected static IServerNetworkChannel ServerChannel { get; private set; }
        protected static bool IsClientLoaded { get; private set; }
#if DEBUG
        public override void StartServerSide(ICoreServerAPI api)
#else
        public void StartServerSide(ICoreServerAPI api)
#endif
        {
            this.serverApi = api;
            if (api.Network.GetChannel(CHANNELNAME) != null)
            {
                return;
            }

            ServerChannel =
                api.Network.RegisterChannel(CHANNELNAME)
                .RegisterMessageType<ClientLoadedRequest>()
                .SetMessageHandler<ClientLoadedRequest>(this.OnClientReady)
                .RegisterMessageType<SerializableCommandRequest>()
                .RegisterMessageType<SerializableCommandResponse>()
                .SetMessageHandler<SerializableCommandRequest>(this.OnClientHelp);

#if DEBUG
            this.debugCommandSystem = new CommandSystem(new HelpConfig(), api);
#endif

            ServerChannel.SendPacket(new ClientLoadedRequest());
        }

        private void OnClientReady(IServerPlayer fromPlayer, ClientLoadedRequest networkMessage)
        {
            IsClientLoaded = true;
        }

        private void OnClientHelp(IServerPlayer fromPlayer, SerializableCommandRequest networkMessage)
        {
            var cs2 = this.serverApi.ObjectCache["CS2System"] as CommandSystem;
            Dictionary<string, string> serverCommands = null;
            try
            {
                serverCommands = cs2.RenderCommandsAsString(cs2.CreateFakeContext(fromPlayer, "exthelp"), networkMessage.Commands).ConfigureAwait(false).GetAwaiter().GetResult();
            }
            catch (VSCommandNotFoundException)
            {
                serverCommands = new Dictionary<string, string>();
            }
            catch (Exception)
            {
                fromPlayer.SendMessage(GlobalConstants.AllChatGroups, "An unknown error occurred.", EnumChatType.CommandError);
                return;
            }

            ServerChannel.SendPacket(new SerializableCommandResponse() { RenderedCommandStrings = serverCommands, RequestedCommand = networkMessage.Commands }, fromPlayer);
        }
        #endregion
    }
}
