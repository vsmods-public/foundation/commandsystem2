namespace CommandSystem2.Entities.VSCommand
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents a command group.
    /// </summary>
    public class CommandGroup : Command
    {
        /// <summary>
        /// Gets all the commands that belong to this module.
        /// </summary>
        public IReadOnlyList<Command> Children { get; internal set; }

        /// <summary>
        /// Gets whether this command is executable without subcommands.
        /// </summary>
        public bool IsExecutableWithoutSubcommands => this.Overloads?.Any() == true;

        internal CommandGroup() : base() { }

        /// <summary>
        /// Executes this command or its subcommand with specified context.
        /// </summary>
        /// <param name="context">Context to execute the command in.</param>
        /// <returns>Command's execution results.</returns>
        public override async Task<CommandResult> ExecuteAsync(CommandContext context)
        {
            var subcommand = context.CommandArguments.PopWord();
            if (subcommand != null)
            {
                var cmd = this.Children.FirstOrDefault(xc => xc.Name.ToLowerInvariant() == subcommand.ToLowerInvariant());

                if (cmd != null)
                {
                    // pass the execution on
                    var newContext = new CommandContext
                    {
                        Command = cmd,
                        CommandArguments = context.CommandArguments,
                        Config = context.Config,
                        GroupId = context.GroupId,
                        ClientPlayer = context.ClientPlayer,
                        ServerPlayer = context.ServerPlayer,
                        CommandSystem = context.CommandSystem,
                        Services = context.Services,
                        ClientAPI = context.ClientAPI,
                        ServerAPI = context.ServerAPI
                    };

                    return await cmd.ExecuteAsync(newContext).ConfigureAwait(false);
                }
            }

            if (!this.IsExecutableWithoutSubcommands)
            {
                return new CommandResult
                {
                    IsSuccessful = false,
                    Exception = new InvalidOperationException("No matching subcommands were found, and this group is not executable."),
                    Context = context
                };
            }

            return await base.ExecuteAsync(context).ConfigureAwait(false);
        }
    }
}
