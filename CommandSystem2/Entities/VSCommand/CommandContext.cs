namespace CommandSystem2.Entities.VSCommand
{
    using CommandSystem2.Entities.Interfaces;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using Vintagestory.API.Client;
    using Vintagestory.API.Common;
    using Vintagestory.API.Server;

    /// <summary>
    /// Represents a context in which a command is executed.
    /// </summary>
    public sealed class CommandContext
    {
        /// <summary>
        /// Gets the server-side player which executed this command. Null on the client.
        /// </summary>
        public IServerPlayer ServerPlayer { get; internal set; }

        /// <summary>
        /// Gets the client-side player which executed this command. Null on the server.
        /// </summary>
        public IClientPlayer ClientPlayer { get; internal set; }

        /// <summary>
        /// Gets the CmdArgs object that was passed along to the original command.
        /// </summary>
        public CmdArgs CommandArguments { get; internal set; }

        /// <summary>
        /// Gets the ID of the group that the player sent this command to.
        /// </summary>
        public int GroupId { get; internal set; }

        /// <summary>
        /// Gets the command that is being executed.
        /// </summary>
        public Command Command { get; internal set; }

        /// <summary>
        /// Gets the overload of the command that is being executed.
        /// </summary>
        public CommandOverload Overload { get; internal set; }

        /// <summary>
        /// Gets the <see cref="CommandSystem2.CommandSystem"/> instance that handled this command.
        /// </summary>
        public CommandSystem CommandSystem { get; internal set; }

        /// <summary>
        /// Gets the <see cref="ICoreClientAPI"/> instance that was used to register this command. Null on the server.
        /// </summary>
        public ICoreClientAPI ClientAPI { get; internal set; }

        /// <summary>
        /// Gets the <see cref="ICoreServerAPI"/> instance that was used to register this command. Null on the client.
        /// </summary>
        public ICoreServerAPI ServerAPI { get; internal set; }

        /// <summary>
        /// Gets the <see cref="ICommandSystemConfig"/> object of the <see cref="ModSystem"/> that registered this <see cref="CommandSystem2.CommandSystem"/> context.
        /// </summary>
        public ICommandSystemConfig Config { get; internal set; }

        /// <summary>
        /// Gets the service provider for this <see cref="CommandSystem"/> instance.
        /// </summary>
        public IServiceProvider Services { get; internal set; }

        /*internal ServiceContext ServiceScopeContext { get; set; }
        internal struct ServiceContext : IDisposable
        {
            public IServiceProvider Provider { get; }
            public IServiceScope Scope { get; }
            public bool IsInitialized { get; }

            public ServiceContext(IServiceProvider services, IServiceScope scope)
            {
                this.Provider = services;
                this.Scope = scope;
                this.IsInitialized = true;
            }

            public void Dispose()
            {
                this.Scope?.Dispose();
            }
        }*/
    }
}
