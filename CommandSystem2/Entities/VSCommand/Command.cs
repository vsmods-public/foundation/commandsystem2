namespace CommandSystem2.Entities.VSCommand
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using CommandSystem2.Attributes.CheckAttributes;
    using CommandSystem2.Extensions;

    /// <summary>
    /// Represents a command.
    /// </summary>
    public class Command
    {
        /// <summary>
        /// Gets this command's name.
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// Gets this command's qualified name (i.e. one that includes all module names).
        /// </summary>
        public string QualifiedName
            => this.Parent != null ? string.Concat(this.Parent.QualifiedName, " ", this.Name) : this.Name;

        /// <summary>
        /// Gets this command's description.
        /// </summary>
        public string Description { get; internal set; }

        /// <summary>
        /// Gets this command's parent module, if any.
        /// </summary>
        public CommandGroup Parent { get; internal set; }

        /// <summary>
        /// Gets a collection of pre-execution checks for this command.
        /// </summary>
        public IReadOnlyList<VSCommandCheckBaseAttribute> ExecutionChecks { get; internal set; }

        /// <summary>
        /// Gets a collection of this command's overloads.
        /// </summary>
        public IReadOnlyList<CommandOverload> Overloads { get; internal set; }

        internal Command()
        {
        }

        /// <summary>
        /// Executes this command with specified context.
        /// </summary>
        /// <param name="context">Context to execute the command in.</param>
        /// <returns>Command's execution results.</returns>
        public virtual async Task<CommandResult> ExecuteAsync(CommandContext context)
        {
            CommandResult result = default;
            try
            {
                var executed = false;
                foreach (var overload in this.Overloads)
                {
                    context.Overload = overload;
                    var arguments = await context.CommandSystem.BindArguments(context, context.Config.IgnoreExtraArguments).ConfigureAwait(false);

                    if (!arguments.IsSuccessful)
                    {
                        continue;
                    }

                    arguments.Converted[0] = overload.InvocationTarget ?? overload.InvocationType.CreateInstance(context.Services);
                    var returnObject = (Task)overload.Callable.DynamicInvoke(arguments.Converted);
                    await returnObject.ConfigureAwait(false);
                    executed = true;
                    result = new CommandResult
                    {
                        IsSuccessful = true,
                        Context = context
                    };

                    break;
                }

                if (!executed)
                {
                    throw new ArgumentException("Could not find a suitable overload for the command.");
                }
            }
            catch (Exception ex)
            {
                result = new CommandResult
                {
                    IsSuccessful = false,
                    Exception = ex,
                    Context = context
                };
            }

            return result;
        }

        /// <summary>
        /// Runs pre-execution checks for this command and returns any that fail for given context.
        /// </summary>
        /// <param name="context">Context in which the command is executed.</param>
        /// <param name="help">Whether this check is being executed from help or not. This can be used to probe whether command can be run without setting off certain fail conditions (such as cooldowns).</param>
        /// <returns>Pre-execution checks that fail for given context.</returns>
        public async Task<IEnumerable<VSCommandCheckBaseAttribute>> RunChecksAsync(CommandContext context, bool help)
        {
            var failedChecks = new List<VSCommandCheckBaseAttribute>();
            if (this.ExecutionChecks != null && this.ExecutionChecks.Any())
            {
                foreach (var check in this.ExecutionChecks)
                {
                    if (!await check.ExecuteCheckAsync(context, help).ConfigureAwait(false))
                    {
                        failedChecks.Add(check);
                    }
                }
            }

            return failedChecks;
        }

        /// <summary>
        /// Checks whether this command is equal to another one.
        /// </summary>
        /// <param name="cmd1">Command to compare to.</param>
        /// <param name="cmd2">Command to compare.</param>
        /// <returns>Whether the two commands are equal.</returns>
        public static bool operator ==(Command cmd1, Command cmd2)
        {
            var o1 = cmd1 as object;
            var o2 = cmd2 as object;

            if (o1 == null && o2 != null)
            {
                return false;
            }
            else if (o1 != null && o2 == null)
            {
                return false;
            }
            else if (o1 == null && o2 == null)
            {
                return true;
            }

            return cmd1.QualifiedName == cmd2.QualifiedName;
        }

        /// <summary>
        /// Checks whether this command is not equal to another one.
        /// </summary>
        /// <param name="cmd1">Command to compare to.</param>
        /// <param name="cmd2">Command to compare.</param>
        /// <returns>Whether the two commands are not equal.</returns>
        public static bool operator !=(Command cmd1, Command cmd2)
        {
            return !(cmd1 == cmd2);
        }

        /// <summary>
        /// Checks whether this command equals another object.
        /// </summary>
        /// <param name="other">Object to compare to.</param>
        /// <returns>Whether this command is equal to another object.</returns>
        public override bool Equals(object other)
        {
            var o1 = other;
            var o2 = this as object;

            if (o1 == null && o2 != null)
            {
                return false;
            }
            else if (o1 != null && o2 == null)
            {
                return false;
            }
            else if (o1 == null && o2 == null)
            {
                return true;
            }

            var cmd = other as Command;
            if ((object)cmd == null)
            {
                return false;
            }

            return cmd.QualifiedName == this.QualifiedName;
        }

        /// <summary>
        /// Gets this command's hash code.
        /// </summary>
        /// <returns>This command's hash code.</returns>
        public override int GetHashCode()
        {
            return this.QualifiedName.GetHashCode();
        }

        /// <summary>
        /// Returns a string representation of this command.
        /// </summary>
        /// <returns>String representation of this command.</returns>
        public override string ToString()
        {
            if (this is CommandGroup g)
            {
                return $"Command Group: {this.QualifiedName}, {g.Children.Count} top-level children";
            }

            return $"Command: {this.QualifiedName}";
        }
    }
}
