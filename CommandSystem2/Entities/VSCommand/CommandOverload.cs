namespace CommandSystem2.Entities.VSCommand
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents a specific overload of a command.
    /// </summary>
    public class CommandOverload
    {
        /// <summary>
        /// Gets this command overload's arguments.
        /// </summary>
        public IReadOnlyList<CommandArgument> Arguments { get; internal set; }

        /// <summary>
        /// Gets this command overload's delegate.
        /// </summary>
        internal Delegate Callable { get; set; }

        internal object InvocationTarget { get; set; }

        internal Type InvocationType { get; set; }

        internal CommandOverload() { }
    }
}
