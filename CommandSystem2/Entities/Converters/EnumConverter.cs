namespace CommandSystem2.Entities.Converters
{
    using System;
    using System.Reflection;
    using System.Threading.Tasks;
    using CommandSystem2.Entities.Interfaces;
    using CommandSystem2.Entities.VSCommand;
    using CommandSystem2.Entities;

    public class EnumConverter<T> : IArgumentConverter<T> where T : struct, IComparable, IConvertible, IFormattable
    {
        Task<Optional<T>> IArgumentConverter<T>.ConvertAsync(string value, CommandContext context)
        {
            var typeInfo = typeof(T).GetTypeInfo();
            if (!typeInfo.IsEnum)
            {
                throw new InvalidOperationException("Cannot convert non-enum value to an enum.");
            }

            if (Enum.TryParse(value, true, out T ev))
            {
                return Task.FromResult(Optional.FromValue(ev));
            }

            return Task.FromResult(Optional.FromNoValue<T>());
        }
    }
}
