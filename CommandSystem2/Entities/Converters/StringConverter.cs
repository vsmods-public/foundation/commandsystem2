namespace CommandSystem2.Entities.Converters
{
    using System;
    using System.Threading.Tasks;
    using CommandSystem2.Entities;
    using CommandSystem2.Entities.Interfaces;
    using CommandSystem2.Entities.VSCommand;

    public class StringConverter : IArgumentConverter<string>
    {
        Task<Optional<string>> IArgumentConverter<string>.ConvertAsync(string value, CommandContext context)
        {
            return Task.FromResult(Optional.FromValue(value));
        }
    }

    public class UriConverter : IArgumentConverter<Uri>
    {
        Task<Optional<Uri>> IArgumentConverter<Uri>.ConvertAsync(string value, CommandContext context)
        {
            try
            {
                if (value.StartsWith("<") && value.EndsWith(">"))
                {
                    value = value.Substring(1, value.Length - 2);
                }

                return Task.FromResult(Optional.FromValue(new Uri(value)));
            }
            catch
            {
                return Task.FromResult(Optional.FromNoValue<Uri>());
            }
        }
    }
}
