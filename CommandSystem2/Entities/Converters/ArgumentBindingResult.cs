namespace CommandSystem2.Entities.Converters
{
    using System;

    internal class ArgumentBindingResult
    {
        public bool IsSuccessful { get; }
        public object[] Converted { get; }
        public Exception Reason { get; }

        public ArgumentBindingResult(object[] converted)
        {
            this.IsSuccessful = true;
            this.Reason = null;
            this.Converted = converted;
        }

        public ArgumentBindingResult(Exception ex)
        {
            this.IsSuccessful = false;
            this.Reason = ex;
            this.Converted = null;
        }
    }
}
