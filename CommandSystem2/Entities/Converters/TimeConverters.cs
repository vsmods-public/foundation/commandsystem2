namespace CommandSystem2.Entities.Converters
{
    using System;
    using System.Globalization;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using CommandSystem2.Entities;
    using CommandSystem2.Entities.Interfaces;
    using CommandSystem2.Entities.VSCommand;

    public class DateTimeConverter : IArgumentConverter<DateTime>
    {
        Task<Optional<DateTime>> IArgumentConverter<DateTime>.ConvertAsync(string value, CommandContext ctx)
        {
            if (DateTime.TryParse(value, CultureInfo.InvariantCulture, DateTimeStyles.None, out var result))
            {
                return Task.FromResult(new Optional<DateTime>(result));
            }

            return Task.FromResult(Optional.FromNoValue<DateTime>());
        }
    }

    public class DateTimeOffsetConverter : IArgumentConverter<DateTimeOffset>
    {
        Task<Optional<DateTimeOffset>> IArgumentConverter<DateTimeOffset>.ConvertAsync(string value, CommandContext ctx)
        {
            if (DateTimeOffset.TryParse(value, CultureInfo.InvariantCulture, DateTimeStyles.None, out var result))
            {
                return Task.FromResult(Optional.FromValue(result));
            }

            return Task.FromResult(Optional.FromNoValue<DateTimeOffset>());
        }
    }

    public class TimeSpanConverter : IArgumentConverter<TimeSpan>
    {
        private static Regex TimeSpanRegex { get; set; }

        static TimeSpanConverter()
        {
#if NETSTANDARD1_3
            TimeSpanRegex = new Regex(@"^(?<days>\d+d\s*)?(?<hours>\d{1,2}h\s*)?(?<minutes>\d{1,2}m\s*)?(?<seconds>\d{1,2}s\s*)?$", RegexOptions.ECMAScript);
#else
            TimeSpanRegex = new Regex(@"^(?<days>\d+d\s*)?(?<hours>\d{1,2}h\s*)?(?<minutes>\d{1,2}m\s*)?(?<seconds>\d{1,2}s\s*)?$", RegexOptions.ECMAScript | RegexOptions.Compiled);
#endif
        }

        Task<Optional<TimeSpan>> IArgumentConverter<TimeSpan>.ConvertAsync(string value, CommandContext context)
        {
            if (value == "0")
            {
                return Task.FromResult(Optional.FromValue(TimeSpan.Zero));
            }

            if (int.TryParse(value, NumberStyles.Number, CultureInfo.InvariantCulture, out _))
            {
                return Task.FromResult(Optional.FromNoValue<TimeSpan>());
            }

            value = value.ToLowerInvariant();

            if (TimeSpan.TryParse(value, CultureInfo.InvariantCulture, out var result))
            {
                return Task.FromResult(Optional.FromValue(result));
            }

            var groups = new string[] { "days", "hours", "minutes", "seconds" };
            var regexMatch = TimeSpanRegex.Match(value);
            if (!regexMatch.Success)
            {
                return Task.FromResult(Optional.FromNoValue<TimeSpan>());
            }

            var d = 0;
            var h = 0;
            var m = 0;
            var s = 0;
            foreach (var group in groups)
            {
                var groupValue = regexMatch.Groups[group].Value;
                if (string.IsNullOrWhiteSpace(groupValue))
                {
                    continue;
                }

                var groupToken = groupValue[groupValue.Length - 1];
                int.TryParse(groupValue.Substring(0, groupValue.Length - 1), NumberStyles.Integer, CultureInfo.InvariantCulture, out var val);
                switch (groupToken)
                {
                    case 'd':
                        d = val;
                        break;

                    case 'h':
                        h = val;
                        break;

                    case 'm':
                        m = val;
                        break;

                    case 's':
                        s = val;
                        break;
                }
            }
            result = new TimeSpan(d, h, m, s);
            return Task.FromResult(Optional.FromValue(result));
        }
    }
}
