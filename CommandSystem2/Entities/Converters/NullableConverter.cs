namespace CommandSystem2.Entities.Converters
{
    using System.Threading.Tasks;
    using CommandSystem2.Entities;
    using CommandSystem2.Entities.Interfaces;
    using CommandSystem2.Entities.VSCommand;

    public class NullableConverter<T> : IArgumentConverter<T?> where T : struct
    {
        async Task<Optional<T?>> IArgumentConverter<T?>.ConvertAsync(string value, CommandContext context)
        {
            value = value?.ToLowerInvariant();

            if (value == null || value == "null")
            {
                return Optional.FromValue<T?>(null);
            }

            if (context.CommandSystem.ArgumentConverters.TryGetValue(typeof(T), out var argumentConverter))
            {
                var typedArgumentConverter = argumentConverter as IArgumentConverter<T>;
                var convertedValue = await typedArgumentConverter.ConvertAsync(value, context).ConfigureAwait(false);
                return convertedValue.HasValue ? Optional.FromValue<T?>(convertedValue.Value) : Optional.FromNoValue<T?>();
            }

            return Optional.FromNoValue<T?>();
        }
    }
}
